
import 'core-js/fn/array/find-index';
import Alert from './components/Alert';
import Icon from './components/Icon';
import {Row, Col} from './components/Grid/';
import Affix from './components/Affix/';
import Button from './components/Button/';
import BackTop from './components/BackTop/';
import Badge from './components/badge/'
import Breadcrumb from './components/breadcrumb/'
import Card from './components/card'
import Message from './components/message';
import Checkbox from './components/checkbox';
import Radio from './components/radio';
import Input from './components/input';
import Switch from './components/switch';
import { Select, Option } from './components/select';

const nicev = {
	Alert,
	Icon,
	Row,
	Col,
	Affix,
	Button,
	Badge,
	ButtonGroup: Button.Group,
	BackTop: BackTop,
	Breadcrumb,
	BreadcrumbItem: Breadcrumb.Item,
	Card,
	Message,
	Checkbox,
	CheckboxGroup: Checkbox.Group,
	Radio,
	RadioGroup: Radio.Group,
	Input,
	iSwitch: Switch,
	Select,
	Option: Option
}

const install = function (Vue, opts = {}) {
    Object.keys(nicev).forEach((key) => {
        Vue.component(key, nicev[key]);
    });
	Vue.prototype.$Message = Message;
};

if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue);
}
// Object.assign() 方法用于将所有可枚举的属性的值从一个或多个源对象复制到目标对象。它将返回目标对象。
export default Object.assign(nicev, {install})
