import Notification from './notification.vue';
import Vue from 'vue';

Notification.newInstance = properties => {
    const Instance = new Vue({
        // properties 组件属性
        data: properties,
        render (h) {
            // 在 createElement 函数中生成模板
            // 第一个参数，可以是一个 HTML 标签字符串, 或者组件选项对象
            // 第二个参数， 一个包含模板相关属性的数据对象
            return h(Notification, {
                props: properties
            });
        }
    });
    // vm.$mount()手动地挂载一个未挂载的实例
    // 返回实例自身
    // 在文档之外渲染并且随后挂载
    const component = Instance.$mount();
    document.body.appendChild(component.$el);
    // vm.$children 当前实例的直接子组件
    // 如果发现自己正在尝试使用 $children 来进行数据绑定，考虑使用一个数组配合 v-for 来生成子组件，并且使用 Array 作为真正的来源
    const notification = Instance.$children[0];

    return {
        notice (noticeProps) {
            notification.add(noticeProps);
        },
        remove (name) {
            notification.close(name);
        },
        component: notification,
        destroy (element) {
            notification.closeAll();
            setTimeout(function() {
                document.body.removeChild(document.getElementsByClassName(element)[0]);
            }, 500);
        }
    }
}

export default Notification;