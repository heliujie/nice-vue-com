import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './app.vue';
import nicev from '../src/index'
import './style/reset.css'
import './style/iview.css'
import Alert from './routers/alert.vue'
import Grid from './routers/grid.vue'
import Affix from './routers/affix.vue'
import Button from './routers/button.vue'
import BackTop from './routers/backTop.vue'
import Badge from './routers/badge.vue'
import Breadcrumb from './routers/breadcrumb.vue'
import Card from './routers/card.vue'
import Message from './routers/message.vue'
import Checkbox from './routers/checkbox.vue'
import Radio from './routers/radio.vue'
import Input from './routers/input.vue'
import Switch from './routers/switch.vue'
import Select from './routers/select.vue'

Vue.use(VueRouter)
Vue.use(nicev)

Vue.config.debug = true;

const router = new VueRouter({
	routes: [
		{
			name: 'alert',
			path: '/alert',
			component: Alert
		},
		{
			name: 'grid',
			path: '/grid',
			component: Grid
		},
		{
			name: 'affix',
			path: '/affix',
			component: Affix
		},
		{
			name: 'button',
			path: '/button',
			component: Button
		},
		{
			name: 'backTop',
			path: '/backTop',
			component: BackTop
		},
		{
			name: 'badge',
			path: '/badge',
			component: Badge
		},
		{
			name: 'breadcrumb',
			path: '/breadcrumb',
			component: Breadcrumb
		},{
			name: 'card',
			path: '/card',
			component: Card
		},{
			name: 'message',
			path: '/message',
			component: Message
		},{
			name: 'checkbox',
			path: '/checkbox',
			component: Checkbox
		},{
			name: 'radio',
			path: '/radio',
			component: Radio
		},{
			name: 'input',
			path: '/input',
			component: Input
		},{
			name: 'switch',
			path: '/switch',
			component: Switch
		},{
			name: 'select',
			path: '/select',
			component: Select
		}
	]
})

new Vue({
    el: '#app',
    router: router,
    render: h => h(App)
});
